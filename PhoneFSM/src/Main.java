import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        System.out.println("Phone FSM demo, Phone begins in OffState");
        Phone myPhone = new Phone();
        JFrame f = new JFrame("Phone");
        JTextField tf = new JTextField();
        tf.setBounds(50,50,400,60);
        JButton home = new JButton("Home");
        JTextField tf2 = new JTextField();
        home.addActionListener(e-> tf.setText(myPhone.clickHome()));
        JButton power = new JButton("Power");
        power.addActionListener(e->tf.setText(myPhone.clickPower()));
        home.setBounds(50,150,95,30);
        power.setBounds(200,150,95,30);
        f.add(tf);
        f.add(home);
        f.add(power);
        f.setSize(600,400);
        f.setLayout(null);
        f.setVisible(true);
//        System.out.println(myPhone.clickHome());
//        System.out.println(myPhone.clickHome());
    }
}