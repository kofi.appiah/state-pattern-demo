public abstract class State {

    public State() {}

    public abstract String homeButton(Phone phone);

    public abstract String powerButton(Phone phone);

}