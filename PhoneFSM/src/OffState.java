public class OffState extends State {

    public OffState() {}

    @Override
    public String homeButton(Phone phone) {
        phone.setState(new LockedState());
        return phone.turnOn();
    }

    @Override
    public String powerButton(Phone phone) {
        phone.setState(new LockedState());
        return phone.turnOn();
    }

}