public class ReadyState extends State {

    public ReadyState(){}

    @Override
    public String homeButton(Phone phone) {
        return phone.home();
    }

    @Override
    public String powerButton(Phone phone) {
        phone.setState(new OffState());
        return phone.lock();
    }

}
