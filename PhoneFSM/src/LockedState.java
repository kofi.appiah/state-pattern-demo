public class LockedState extends State {

    public LockedState(){}

    @Override
    public String homeButton(Phone phone) {
        phone.setState(new ReadyState());
        return phone.unlock();
    }

    @Override
    public String powerButton(Phone phone) {
        phone.setState(new OffState());
        return phone.lock();
    }

}