package Nostatepattern;

public class PhoneIF {
    private boolean isOff = true;
    private boolean isLocked = false;
    private boolean isOn = false;

    public PhoneIF(){
    }


    public String lock() {
        return "Locking phone and turning off the screen";
    }

    public String home() {
        return "Going to home-screen";
    }

    public String unlock() {
        return "Unlocking the phone to home";
    }

    public String turnOn() {
        return "Turning screen on, device still locked";
    }

    public String clickHome() {
        String homeCommand = "";

        if (isOff){
            homeCommand = this.turnOn();
            this.isLocked = true;
            this.isOff = false;
        }

        else if (isLocked){
            homeCommand = this.unlock();
            this.isOn = true;
            this.isLocked = false;
        }

        else if (isOn){
            homeCommand = this.home();
        }

        return homeCommand;
    }

    public String clickPower() {
        String powerCommand = "";

        if (isOff){
            powerCommand = this.turnOn();
            this.isLocked = true;
            this.isOff = false;
        }

        else if (isLocked){
            powerCommand = this.lock();
            this.isOff = true;
            this.isLocked = false;
        }

        else if (isOn){
            powerCommand = this.lock();
            this.isOff = true;
            this.isOn = false;
        }

        return powerCommand;
    }
}
